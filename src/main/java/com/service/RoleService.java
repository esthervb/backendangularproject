package com.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.entity.Role;


public interface RoleService {
	
	public Role addOrUpdateRole(Role role);
	
	public Role getRole(Long id);
	
	public void deleteRole(Long id);
	
	public List<Role> getAllRoles();

}
