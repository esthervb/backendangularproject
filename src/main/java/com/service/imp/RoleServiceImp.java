package com.service.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.Role;
import com.repository.RoleRepository;
import com.service.RoleService;
@Service
public class RoleServiceImp implements RoleService{
	
	@Autowired
	RoleRepository roleDao;

	@Override
	public Role addOrUpdateRole(Role role) {
		// TODO Auto-generated method stub
		return roleDao.save(role);
	}

	@Override
	public Role getRole(Long id) {
		// TODO Auto-generated method stub
		return roleDao.findById(id).get();
	}

	@Override
	public void deleteRole(Long id) {
		// TODO Auto-generated method stub
		roleDao.deleteById(id);
	}

	@Override
	public List<Role> getAllRoles() {
		// TODO Auto-generated method stub
		return roleDao.findAll();
	}

}
