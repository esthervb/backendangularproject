package com.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.entity.User;


public interface UserService {
	
	public User addOrUpdateUser(User user);
	
	public User getUser(Long id);
	
	public void deleteUser(Long id);
	
	public List<User> getAllUsers();

}
