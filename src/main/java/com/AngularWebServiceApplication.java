package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AngularWebServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(AngularWebServiceApplication.class, args);
	}

}
